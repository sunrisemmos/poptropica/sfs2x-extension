function init()
{
    addEventHandler(SFSEventType.USER_JOIN_ZONE, onJoinZone);
    addEventHandler(SFSEventType.USER_LOGIN, onNewLogin);
    addRequestHandler("UC", onUserUpdate);
    addRequestHandler("joinScene", onJoinScene);
}

function destroy()
{
}

function onNewLogin(event)
{
    user = event.getParameter(SFSEventParam.LOGIN_NAME);
}

function onJoinZone(event)
{
}

function joinUserInRoom(user, room, roomType)
{
    var roomType = new SFSRoomVariable("type", roomType, VariableType.STRING);
    roomType.setGlobal(true);

    getApi().setRoomVariables(null, room, [roomType], true);

    var outParams = new SFSObject();

    // Send response back to client
    // send("joinScene", outParams, [sender]);

    getApi().joinRoom(user, room, "", false, null, true, false);
    trace(room.getUserList());
}

function onUserUpdate(inParams, sender)
{
    // trace(inParams.toJson());

    var userData = [];
    userData.push(new SFSUserVariable("char_look", inParams.getUtfString("char_look")));
    getApi().setUserVariables(sender, userData, true, false);

    var outParams = new SFSObject();

    // Send response back to client
    send("UC", outParams, sender);
}

function onJoinScene(inParams, sender)
{
    trace(inParams.toJson());

    joinUserInRoom(sender, getParentZone().getRoomByName("Test"), inParams.getUtfString("type"));
}