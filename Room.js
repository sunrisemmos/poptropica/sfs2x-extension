function init()
{
    trace("Loading room!");
    addRequestHandler("F", onUpdateFreq);
    addRequestHandler("U", onUpdatePlayer);

}
function onUpdateFreq(inParams, sender) {
    // trace(inParams.toJson());
}

function onUpdatePlayer(inParams, sender) {
    // trace(inParams.toJson());

    var outParams = new SFSObject();

    getApi().sendPublicMessage(sender.getLastJoinedRoom(), sender, "A", outParams);
}

function destroy()
{
}